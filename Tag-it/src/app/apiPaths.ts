import { RequestMethod } from "@angular/http";

export const Paths={
    'add-item':{
        path:'/api/Items',
        method:RequestMethod.Post
    },
    'edit-item':{
        path:'/api/Items/:id',
        method:RequestMethod.Put
    },
    'delete-item':{
        path:'/api/Items/:id',
        method:RequestMethod.Delete
    },
    'selectAll-item':{
        path:'/api/Items',
        method:RequestMethod.Get
    },
    'selectOne-item':{
        path:'/api/Items/:id',
        method:RequestMethod.Get
    },
    'uploadImg-item':{
        path:'/UploadImage',
        method:RequestMethod.Get
    },
}
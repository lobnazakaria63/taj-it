import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { Items } from './interfaces';
import { error } from 'util';
import { Observable } from 'rxjs';

@Injectable()
export class ItemService {
getItemUrl="http://task.taj-it.com/api/Items";
editItemUrl="http://task.taj-it.com/api/Items/";
upImg="http://task.taj-it.com/UploadImage/";
option:RequestOptions;
    constructor(private http: Http) {}

    getItems() {
        return this.http.get(this.getItemUrl)
    }
    editItem(id,item):Observable<Items>{
        debugger
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers});
        options.method=RequestMethod.Put;
        options.body=item;
        options.url=this.editItemUrl+id;
        return this.http.request(options.url,options)
        .map(success => success.status)
        .catch(this.handleError)
    }
    getOneItem(id){
        return this.http.get(this.editItemUrl + id)
    }
    addItem(item,option:RequestOptions){
        
       
        option.headers= new Headers();
        option.headers.append('Content-Type','application/json');
        option.method=RequestMethod.Post;
        option.url=this.getItemUrl;
        option.body=JSON.stringify(item);
        return this.http.request(option.url, option)
    }
    deleteItem(id){
        return this.http.delete(this.editItemUrl + id);
    }
    private handleError (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.status);
        }
}
import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { RequestMethod, Http } from "@angular/http";
import 'rxjs/Rx';
import { ItemService } from './item.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  editItemUrl="http://task.taj-it.com/api/Items/";
  selectedFile: File;
  p:number;
  term:string;
  form:string;
  items:any[]=[];
  submetNew:boolean;
  submetUpdate:boolean;
  itemForm:FormGroup;
  modalRef: BsModalRef;
  modalRef2: BsModalRef;
  itemId:number;
  itemName:string;
  itemPrice:number;
  img2:string;
  img1:string="http://task.taj-it.com/UploadImage/";
  itemImg:string;
  itemDate=new Date;
  inv:number[]=[];
  constructor(private itemService:ItemService,
    private http:Http,
    private modalService: BsModalService,
    private fb:FormBuilder,
    private spinner: NgxSpinnerService
   ){
}
ngOnInit(){
  this.spinner.show();
this.itemService.getItems().subscribe(data =>
  { this.items.push(data.json())
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
  }, 5000);
  });

this.itemForm=this.fb.group({
  ItemId:[0],
  ItemName:['',Validators.maxLength(50)],
  Price:[0],
  Photo:[''],
  ExpireDate:[''],
  InvoiceDetails:['']
})
}
openModal(template: TemplateRef<any>,id?:number) {
  if(id){
    this.form="Edit Item of ID =" +id;
  this.itemId=id;
  this.submetUpdate=true;
  this.submetNew=false;
  this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  this.itemService.getOneItem(id).subscribe(data =>{
    this.itemName=data.json().ItemName;
    this.itemPrice=data.json().Price;
    this.itemImg=data.json().Photo;
    this.itemDate=data.json().ExpireDate;
    console.log(this.itemDate)
   //console.log("data item= ", this.itemName, " ",this.itemPrice, " ",this.itemDate)
  })
}
else{
  this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  this.form="Add new Item"
  this.submetUpdate=false;
  this.submetNew=true;
  this.itemDate=new Date;
  this.itemImg="";
  this.itemName="";
  this.itemPrice=null;
  }
}
openModal2(template: TemplateRef<any>,id?:number) {
  if(id){
    this.itemId=id;
    this.modalRef2 = this.modalService.show(template, { class: 'modal-sm' });
  }
  this.modalRef2 = this.modalService.show(template, { class: 'modal-sm' });
}
onSubmitUpddate() {
  debugger
  this.itemForm.controls['ItemId'].setValue(this.itemId);
  this.itemForm.controls['Photo'].setValue(this.itemImg);
  this.itemForm.controls['InvoiceDetails'].setValue(this.inv)
  const result=  this.itemForm.value;
  this.spinner.show();
  this.itemService.editItem(this.itemId , result).subscribe(res=>{
    console.log(res)
    this.modalRef.hide();
  this.modalRef = null;
  this.modalRef2.hide();
  this.modalRef2 = null;
  window.location.reload()
  setTimeout(() => {
    /** spinner ends after 5 seconds */
    this.spinner.hide();
}, 5000);
  })
}
onSubmitNew(){
  
  this.itemForm.controls['Photo'].setValue(this.itemImg);
  const result= Object.assign({}, this.itemForm.value);
  console.log(result)
  let opts;
  opts={
    method:RequestMethod.Post,
  }
  this.spinner.show();
  this.itemService.addItem(result,opts).subscribe(res=>{
    this.modalRef2.hide();
  this.modalRef2 = null;
    this.modalRef.hide();
  this.modalRef = null;
  window.location.reload()
  setTimeout(() => {
    /** spinner ends after 5 seconds */
    this.spinner.hide();
}, 5000);
  })
}
deleteItem(id){
  
  this.itemId=id;
  this.spinner.show();
 this.itemService.deleteItem(id).subscribe(res=>{
  this.modalRef2.hide();
  this.modalRef2 = null;
  window.location.reload()
  setTimeout(() => {
    /** spinner ends after 5 seconds */
    this.spinner.hide();
}, 5000);
 });
}


onFileChanged(event) {
  this.selectedFile = event.target.files[0];
  var pattern = /image-*/;
  var reader = new FileReader();
  if (!this.selectedFile.type.match(pattern)) {
        console.error('File is not an image');
        //of course you can show an alert message here
        return;
    }
    this.img2=this.selectedFile.name;
    this.itemImg=this.img1+this.img2;
  console.log("image= ",this.itemImg )
}
confirmEdit(){
  this.onSubmitUpddate();
}
declineEdit(){
  this.modalRef.hide();
  this.modalRef = null;
  this.modalRef2.hide();
  this.modalRef2 = null;
}
confirmAdd(){
this.onSubmitNew()
}
declineAdd(){
  this.modalRef2.hide();
  this.modalRef2 = null;
  this.modalRef.hide();
  this.modalRef = null;
}
confirmDel(){
this.deleteItem(this.itemId)
}
declineDel(){
  this.modalRef2.hide();
  this.modalRef2 = null;
}

}

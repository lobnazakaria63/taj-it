import{RequestMethod} from '@angular/http';
export interface Ipath{
    path:string;
    method:RequestMethod;
}
export interface Items{
    ItemId:number;
    ItemName:string;
    Price:number;
    Photo:string;
    ExpireDate:string;
    InvoiceDetails:any;
}